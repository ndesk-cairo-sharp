using System;

namespace Cairo
{
	public class FontFace : IDisposable
	{
		readonly IntPtr handle;

		internal static FontFace Lookup (IntPtr handle, bool owns)
		{
			if (handle == IntPtr.Zero)
				return null;

			if (!owns)
				handle = NativeMethods.cairo_font_face_reference (handle);

			return new FontFace (handle);
		}

		//TODO: make non-public when all entry points are complete in binding
		public FontFace (IntPtr handle)
		{
			this.handle = handle;
		}

		~FontFace ()
		{
			Dispose (false);
		}

		void IDisposable.Dispose ()
		{
			Dispose (true);
			GC.SuppressFinalize (this);
		}

		void Dispose (bool disposing)
		{
			NativeMethods.cairo_font_face_destroy (handle);
		}

		public IntPtr Handle {
			get { return handle; }
		}

		public Status Status {
			get { return NativeMethods.cairo_font_face_status (handle); }
		}

		public FontType FontType {
			get { return NativeMethods.cairo_font_face_get_type (handle); }
		}
	}
}
