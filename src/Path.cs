using System;
using System.Runtime.InteropServices;

//TODO: Path iteration
namespace Cairo
{
	public struct Path
	{
		public Status Status;
		public PathData[] Data;
		public int NumData;
	}

	public struct PathData
	{
		public PathDataType DataType;
		public int Length;
		//OR
		public double X, Y;
	}

	public enum PathDataType
	{
		MoveTo,
		LineTo,
		CurveTo,
		ClosePath,
	}
}
