//
// Mono.Cairo.Glyph.cs
//
// Authors: Duncan Mak (duncan@ximian.com)
//          Hisham Mardam Bey (hisham.mardambey@gmail.com)
//
// (C) Ximian, Inc. 2003
// Copyright (C) 2004 Novell, Inc (http://www.novell.com)
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Runtime.InteropServices;

namespace Cairo
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Glyph
	{
		static int native_glyph_size, c_compiler_long_size;

		static Glyph ()
		{
			//
			// This is used to determine what kind of structure
			// we should use to marshal Glyphs, as the public
			// definition in Cairo uses `long', which can be
			// 32 bits or 64 bits depending on the platform.
			//
			// We assume that sizeof(long) == sizeof(void*)
			// except in the case of Win64 where sizeof(long)
			// is 32 bits
			//
			int ptr_size = Marshal.SizeOf (typeof (IntPtr));

			PlatformID platform = Environment.OSVersion.Platform;
			if (platform == PlatformID.Win32NT ||
					platform == PlatformID.Win32S ||
					platform == PlatformID.Win32Windows ||
					platform == PlatformID.WinCE ||
					ptr_size == 4){
				c_compiler_long_size = 4;
				native_glyph_size = Marshal.SizeOf (typeof (NativeGlyph_4byte_longs));
			} else {
				c_compiler_long_size = 8;
				native_glyph_size = Marshal.SizeOf (typeof (Glyph));
			}
		}

		internal long index;
		internal double x;
		internal double y;
		
		public Glyph (long index, double x, double y)
		{
			this.index = index;
			this.x = x;
			this.y = y;
		}
		
		public long Index {
			get { return index; }
			set { index = value; }
		}
		
		public double X {
			get { return x; }
			set { x = value; }
		}
		
		public double Y {
			get { return y; }
			set { y = value; }
		}

		public override bool Equals (object obj)
		{
			if (obj is Glyph)
				return this == (Glyph)obj;
			return false;
		}

		public override int GetHashCode ()
		{
			return (int) Index ^ (int) X ^ (int) Y;
		}

		/*
		internal static IntPtr FromGlyphToUnManagedMemory (Glyph[] glyphs)
		{
			int size = Marshal.SizeOf (glyphs[0]);
			IntPtr dest = Marshal.AllocHGlobal (size * glyphs.Length);
			int pos = dest.ToInt32 ();
			for (int i = 0; i < glyphs.Length; i++, pos += size)
				Marshal.StructureToPtr (glyphs[i], (IntPtr) pos, false);
			return dest;
		}
		*/

		static internal IntPtr FromGlyphToUnManagedMemory (Glyph [] glyphs)
		{
			IntPtr dest = Marshal.AllocHGlobal (native_glyph_size * glyphs.Length);
			int pos = dest.ToInt32 ();

			if (c_compiler_long_size == 8){
				foreach (Glyph g in glyphs){
					Marshal.StructureToPtr (g, (IntPtr)pos, false);
					pos += native_glyph_size;
				}
			} else {
				foreach (Glyph g in glyphs){
					NativeGlyph_4byte_longs n = new NativeGlyph_4byte_longs (g);

					Marshal.StructureToPtr (n, (IntPtr)pos, false);
					pos += native_glyph_size;
				}
			}

			return dest;
		}

		public static bool operator == (Glyph glyph, Glyph other)
		{
			return glyph.Index == other.Index && glyph.X == other.X && glyph.Y == other.Y;
		}

		public static bool operator != (Glyph glyph, Glyph other)
		{
			return !(glyph == other);
		}
	}

	[StructLayout(LayoutKind.Sequential)]
	internal struct NativeGlyph_4byte_longs
	{
		public int index;
		public double x;
		public double y;

		public NativeGlyph_4byte_longs (Glyph source)
		{
			index = (int) source.index;
			x = source.x;
			y = source.y;
		}
	}
}
