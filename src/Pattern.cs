//                                                   
// Mono.Cairo.Pattern.cs
//
// Author: Jordi Mas (jordi@ximian.com)
//         Hisham Mardam Bey (hisham.mardambey@gmail.com)
// (C) Ximian Inc, 2004.
//
// Copyright (C) 2004 Novell, Inc (http://www.novell.com)
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;

namespace Cairo
{
	public class Pattern : IDisposable
	{
		//TODO: review memory management and make handle readonly
		protected IntPtr handle = IntPtr.Zero;

		internal static Pattern Lookup (IntPtr handle, bool owns)
		{
			if (handle == IntPtr.Zero)
				return null;

			if (!owns)
				handle = NativeMethods.cairo_pattern_reference (handle);

			PatternType pt = NativeMethods.cairo_pattern_get_type (handle);
			switch (pt) {
				case PatternType.Solid:
					return new SolidPattern (handle);
				case PatternType.Surface:
					return new SurfacePattern (handle);
				case PatternType.Linear:
					return new LinearGradient (handle);
				case PatternType.Radial:
					return new RadialGradient (handle);
				default:
					return new Pattern (handle);
			}
		}

		protected Pattern ()
		{
		}

		protected Pattern (IntPtr ptr)
		{			
			handle = ptr;
		}		

		~Pattern ()
		{
			Dispose (false);
		}

		void IDisposable.Dispose ()
		{
			Dispose (true);
			GC.SuppressFinalize (this);
		}

		void Dispose (bool disposing)
		{
			if (handle == IntPtr.Zero)
				return;

			NativeMethods.cairo_pattern_destroy (handle);
			handle = IntPtr.Zero;
		}

		//TODO: review Equals and GetHashCode
		public override bool Equals (object o)
		{
			Pattern other = o as Pattern;

			if (o == null)
				return false;

			if (this.handle != other.handle)
				return false;

			return true;
		}

		public override int GetHashCode ()
		{
			return (int)handle ^ GetType ().GetHashCode ();
		}

		public Status Status
		{
			get { return NativeMethods.cairo_pattern_status (handle); }
		}

		public Matrix Matrix {
			set { 
				NativeMethods.cairo_pattern_set_matrix (handle, value);
			}

			get {
				Matrix m = new Matrix ();
				NativeMethods.cairo_pattern_get_matrix (handle, m);
				return m;
			}
		}

		public IntPtr Handle {
			get { return handle; }
		}		

		public PatternType PatternType {
			get { return NativeMethods.cairo_pattern_get_type (handle); }
		}

#if COMPAT
		[Obsolete ("Use the SurfacePattern constructor")]
		public Pattern (Surface surface)
		{
			handle = NativeMethods.cairo_pattern_create_for_surface (surface.Handle);
		}

		//FIXME: needs better explanation
		[Obsolete ("???")]
		public void Destroy ()
		{
			Dispose (false);
		}

		[Obsolete ("Use Handle instead.")]
		public IntPtr Pointer {
			get {
				return Handle;
			}
		}
#endif
	}
}
