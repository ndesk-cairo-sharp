//                                                   
// Mono.Cairo.Pattern.cs
//
// Author: Jordi Mas (jordi@ximian.com)
//         Hisham Mardam Bey (hisham.mardambey@gmail.com)
// (C) Ximian Inc, 2004.
//
// Copyright (C) 2004 Novell, Inc (http://www.novell.com)
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;

namespace Cairo
{
	public sealed class SurfacePattern : Pattern
	{
		internal SurfacePattern (IntPtr handle) : base (handle)
		{
		}

		public SurfacePattern (Surface surface)
		{
			handle = NativeMethods.cairo_pattern_create_for_surface (surface.Handle);
		}

		public Extend Extend {
			set { NativeMethods.cairo_pattern_set_extend (handle, value); }
			get { return NativeMethods.cairo_pattern_get_extend (handle); }
		}

		public Filter Filter {
			set { NativeMethods.cairo_pattern_set_filter (handle, value); }
			get { return NativeMethods.cairo_pattern_get_filter (handle); }
		}

#if CAIRO_1_4
		public Surface Surface {
			get {
				IntPtr surfacePtr;
				Status ret = NativeMethods.cairo_pattern_get_surface (handle, out surfacePtr);
				//TODO: check ret

				Surface retSurface = Surface.Lookup (surfacePtr, false);
				return retSurface;
			}
		}
#endif
	}
}
