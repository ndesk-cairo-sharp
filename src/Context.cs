//
// Mono.Cairo.Context.cs
//
// Author:
//   Duncan Mak (duncan@ximian.com)
//   Miguel de Icaza (miguel@novell.com)
//   Hisham Mardam Bey (hisham.mardambey@gmail.com)
//   Alp Toker (alp@atoker.com)
//
// (C) Ximian Inc, 2003.
// (C) Novell Inc, 2003.
//
// This is an OO wrapper API for the Cairo API.
//
// Copyright (C) 2004 Novell, Inc (http://www.novell.com)
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

using System;
using System.Runtime.InteropServices;

namespace Cairo
{
	public class Context : IDisposable 
	{
		internal readonly IntPtr state;

		public Context (Surface surface)
		{
			state = NativeMethods.cairo_create (surface.Handle);
		}

		public Context (IntPtr state)
		{
			this.state = state;
		}

		~Context ()
		{
			Dispose (false);
		}

		void IDisposable.Dispose ()
		{
			Dispose (true);
			GC.SuppressFinalize (this);
		}

		void Dispose (bool disposing)
		{
			//if Dispose was not called explicitly, we set_source to null to put the context into an error state to avoid threading issues as the GC uses its own thread
			//TODO: write tests to see if this actually makes a difference
			if (!disposing)
				NativeMethods.cairo_set_source (state, IntPtr.Zero);

			NativeMethods.cairo_destroy (state);
		}

		public void Save ()
		{
			NativeMethods.cairo_save (state);
		}

		public void Restore ()
		{
			NativeMethods.cairo_restore (state);
		}

		public Antialias Antialias {
			get { return NativeMethods.cairo_get_antialias (state); }
			set { NativeMethods.cairo_set_antialias (state, value); }
		}

		public Status Status {
			get {
				return NativeMethods.cairo_status (state);
			}
		}

		public IntPtr Handle {
			get {
				return state;
			}
		}

		public Operator Operator {
			set {
				NativeMethods.cairo_set_operator (state, value);
			}

			get {
				return NativeMethods.cairo_get_operator (state);
			}
		}

		public double Tolerance {
			get { return NativeMethods.cairo_get_tolerance (state); }
			set {
				NativeMethods.cairo_set_tolerance (state, value);
			}
		}

		public FillRule FillRule {
			set {
				NativeMethods.cairo_set_fill_rule (state, value);
			}

			get {
				return NativeMethods.cairo_get_fill_rule (state);
			}
		}

		public double LineWidth {
			set {
				NativeMethods.cairo_set_line_width (state, value);
			}

			get {
				return NativeMethods.cairo_get_line_width (state);
			}
		}

		public LineCap LineCap {
			set {
				NativeMethods.cairo_set_line_cap (state, value);
			}

			get {
				return NativeMethods.cairo_get_line_cap (state);
			}
		}

		public LineJoin LineJoin {
			set {
				NativeMethods.cairo_set_line_join (state, value);
			}

			get {
				return NativeMethods.cairo_get_line_join (state);
			}
		}

		public void SetDash (double [] dashes, double offset)
		{
			NativeMethods.cairo_set_dash (state, dashes, dashes.Length, offset);
		}

		//TODO: should be possible to set null on this prop (and elsewhere)?
		//TODO: Source prop setter duplicates SetSource overload right now
		public Pattern Source {
			set {
				SetSource (value);
			}

			get {
				return Pattern.Lookup (NativeMethods.cairo_get_source (state), false);
			}
		}

		public double MiterLimit {
			set {
				NativeMethods.cairo_set_miter_limit (state, value);
			}

			get {
				return NativeMethods.cairo_get_miter_limit (state);
			}
		}

		public PointD CurrentPoint {
			get {
				double x, y;
				NativeMethods.cairo_get_current_point (state, out x, out y);
				return new PointD (x, y);
			}
		}

#if COMPAT
		[Obsolete ("Target setter (but not getter) is obsolete: Create a new Context instead")]
#endif
		public Surface Target {
			get {
				//pass owns=false since cairo owns the surface, not us
				return Surface.Lookup (NativeMethods.cairo_get_target (state), false);
			}
			/*
#if COMPAT
			set {
				NativeMethods.cairo_destroy (state);
				state = NativeMethods.cairo_create (value.Handle);
			}
#endif
			*/
		}

		public void SetSource (Pattern source)
		{
			NativeMethods.cairo_set_source (state, source.Handle);
		}

		//TODO: this api should make it clear which of _rgb and _rgba it calls; needs more thought
		public void SetSource (Color color)
		{
			SetSourceColor (color.R, color.G, color.B, color.A);
		}

		public void SetSourceColor (double red, double green, double blue)
		{
			NativeMethods.cairo_set_source_rgb (state, red, green, blue);
		}

		public void SetSourceColor (double red, double green, double blue, double alpha)
		{
			NativeMethods.cairo_set_source_rgba (state, red, green, blue, alpha);
		}

		public void SetSource (Surface source, double x, double y)
		{
			NativeMethods.cairo_set_source_surface (state, source.Handle, x, y);
		}

		public void SetSource (Surface source, PointD origin)
		{
			SetSource (source, origin.X, origin.Y);
		}

		public void SetSource (Surface source)
		{
			SetSource (source, 0, 0);
		}

		public void NewPath ()
		{
			NativeMethods.cairo_new_path (state);
		}

		public void NewSubPath ()
		{
			NativeMethods.cairo_new_sub_path (state);
		}

		public void MoveTo (PointD p)
		{
			MoveTo (p.X, p.Y);
		}

		public void MoveTo (double x, double y)
		{
			NativeMethods.cairo_move_to (state, x, y);
		}

		public void LineTo (PointD p)
		{
			LineTo (p.X, p.Y);
		}

		public void LineTo (double x, double y)
		{
			NativeMethods.cairo_line_to (state, x, y);
		}

		public void CurveTo (PointD p1, PointD p2, PointD p3)
		{
			CurveTo (p1.X, p1.Y, p2.X, p2.Y, p3.X, p3.Y);
		}

		public void CurveTo (double x1, double y1, double x2, double y2, double x3, double y3)
		{
			NativeMethods.cairo_curve_to (state, x1, y1, x2, y2, x3, y3);
		}

		public void RelMoveTo (Distance d)
		{
			RelMoveTo (d.Dx, d.Dy);
		}

		public void RelMoveTo (double dx, double dy)
		{
			NativeMethods.cairo_rel_move_to (state, dx, dy);
		}

		public void RelLineTo (Distance d)
		{
			RelLineTo (d.Dx, d.Dy);
		}

		public void RelLineTo (double dx, double dy)
		{
			NativeMethods.cairo_rel_line_to (state, dx, dy);
		}

		public void RelCurveTo (Distance d1, Distance d2, Distance d3)
		{
			RelCurveTo (d1.Dx, d1.Dy, d2.Dx, d2.Dy, d3.Dx, d3.Dy);
		}

		public void RelCurveTo (double dx1, double dy1, double dx2, double dy2, double dx3, double dy3)
		{
			NativeMethods.cairo_rel_curve_to (state, dx1, dy1, dx2, dy2, dx3, dy3); 
		}

		public void Arc (double xc, double yc, double radius, double angle1, double angle2)
		{
			NativeMethods.cairo_arc (state, xc, yc, radius, angle1, angle2);
		}

		public void ArcNegative (double xc, double yc, double radius, double angle1, double angle2)
		{
			NativeMethods.cairo_arc_negative (state, xc, yc, radius, angle1, angle2);
		}

		public void Rectangle (Rectangle rectangle)
		{
			Rectangle (rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
		}

		public void Rectangle (PointD p, double width, double height)
		{
			Rectangle (p.X, p.Y, width, height);
		}

		public void Rectangle (double x, double y, double width, double height)
		{
			NativeMethods.cairo_rectangle (state, x, y, width, height);
		}

		public void ClosePath ()
		{
			NativeMethods.cairo_close_path (state);
		}

		public void Paint ()
		{
			NativeMethods.cairo_paint (state);
		}

		public void PaintWithAlpha (double alpha)
		{
			NativeMethods.cairo_paint_with_alpha (state, alpha);
		}

		public void Mask (Pattern pattern)
		{
			NativeMethods.cairo_mask (state, pattern.Handle);
		}

		public void Mask (Surface surface, double surface_x, double surface_y)
		{
			NativeMethods.cairo_mask_surface (state, surface.Handle, surface_x, surface_y);
		}

		public void Stroke ()
		{
			NativeMethods.cairo_stroke (state);
		}

		public void StrokePreserve ()
		{
			NativeMethods.cairo_stroke_preserve (state);
		}		

		public Rectangle StrokeExtents ()
		{
			double x1, y1, x2, y2;
			NativeMethods.cairo_stroke_extents (state, out x1, out y1, out x2, out y2);
			return new Rectangle (x1, y1, x2, y2);
		}

		public void Fill ()
		{
			NativeMethods.cairo_fill (state);
		}

		public Rectangle FillExtents ()
		{
			double x1, y1, x2, y2;
			NativeMethods.cairo_fill_extents (state, out x1, out y1, out x2, out y2);
			return new Rectangle (x1, y1, x2, y2);
		}

		public void FillPreserve ()
		{
			NativeMethods.cairo_fill_preserve (state);
		}

		public void Clip ()
		{
			NativeMethods.cairo_clip (state);
		}

		public void ClipPreserve ()
		{
			NativeMethods.cairo_clip_preserve (state);
		}

		public void ResetClip ()
		{
			NativeMethods.cairo_reset_clip (state);
		}

		public bool InStroke (double x, double y)
		{
			return NativeMethods.cairo_in_stroke (state, x, y);
		}

		public bool InFill (double x, double y)
		{
			return NativeMethods.cairo_in_fill (state, x, y);
		}

		public Pattern PopGroup ()
		{
			return Pattern.Lookup (NativeMethods.cairo_pop_group (state), true);
		}

		public void PopGroupToSource ()
		{
			NativeMethods.cairo_pop_group_to_source (state);
		}

		public void PushGroup ()
		{
			NativeMethods.cairo_push_group (state);
		}

		public void PushGroup (Content content)
		{
			NativeMethods.cairo_push_group_with_content (state, content);
		}

		public Surface GroupTarget {
			get {
				IntPtr surface = NativeMethods.cairo_get_group_target (state);
				//pass owns=false since cairo owns the surface, not us
				return Surface.Lookup (surface, false);
			}
		}

		public void Rotate (double angle)
		{
			NativeMethods.cairo_rotate (state, angle);
		}

		public void Scale (double sx, double sy)
		{
			NativeMethods.cairo_scale (state, sx, sy);
		}

		public void Translate (double tx, double ty)
		{
			NativeMethods.cairo_translate (state, tx, ty);
		}

		public void Transform (Matrix m)
		{
			NativeMethods.cairo_transform (state, m);
		}


		public void UserToDevice (ref double x, ref double y)
		{
			NativeMethods.cairo_user_to_device (state, ref x, ref y);
		}

		public void UserToDeviceDistance (ref double dx, ref double dy) 
		{
			NativeMethods.cairo_user_to_device_distance (state, ref dx, ref dy);
		}

		public void DeviceToUser (ref double x, ref double y)
		{
			NativeMethods.cairo_device_to_user (state, ref x, ref y);
		}

		public void DeviceToUserDistance (ref double dx, ref double dy)
		{
			NativeMethods.cairo_device_to_user_distance (state, ref dx, ref dy);
		}

		public Matrix Matrix {
			set {
				NativeMethods.cairo_set_matrix (state, value);
			}

			get {
				Matrix m = new Matrix ();
				NativeMethods.cairo_get_matrix (state, m);
				return m;
			}
		}

		public void IdentityMatrix ()
		{
			NativeMethods.cairo_identity_matrix (state);
		}

		public void SetFontSize (double scale)
		{
			NativeMethods.cairo_set_font_size (state, scale);
		}

		[Obsolete ("Use SetFontSize() instead.")]
			public double FontSize {
				set { SetFontSize (value); }
			}

		public Matrix FontMatrix {
			get { return NativeMethods.cairo_get_font_matrix (state); }
			set { NativeMethods.cairo_set_font_matrix (state, value); }
		}

		public FontOptions FontOptions {
			get {
				FontOptions options = new FontOptions ();
				NativeMethods.cairo_get_font_options (state, options.Handle);
				return options;
			}
			set { NativeMethods.cairo_set_font_options (state, value.Handle); }
		}

		public void ShowGlyphs (Glyph[] glyphs)
		{
			IntPtr ptr = Glyph.FromGlyphToUnManagedMemory (glyphs);
			NativeMethods.cairo_show_glyphs (state, ptr, glyphs.Length);
			Marshal.FreeHGlobal (ptr);		
		}

		public void GlyphPath (Glyph[] glyphs)
		{
			IntPtr ptr = Glyph.FromGlyphToUnManagedMemory (glyphs);
			NativeMethods.cairo_glyph_path (state, ptr, glyphs.Length);
			Marshal.FreeHGlobal (ptr);

		}

		public FontExtents FontExtents {
			get {
				FontExtents f_extents;
				NativeMethods.cairo_font_extents (state, out f_extents);
				return f_extents;
			}
		}

		public void CopyPage ()
		{
			NativeMethods.cairo_copy_page (state);
		}

		public FontFace FontFace
		{
			get {
				return FontFace.Lookup (NativeMethods.cairo_get_font_face (state), false);
			}
			set {
				NativeMethods.cairo_set_font_face (state, value == null ? IntPtr.Zero : value.Handle);
			}
		}

		public void SelectFontFace (string family, FontSlant slant, FontWeight weight)
		{
			NativeMethods.cairo_select_font_face (state, family, slant, weight);
		}

		public void ShowPage ()
		{
			NativeMethods.cairo_show_page (state);
		}

		public void ShowText (string str)
		{
			NativeMethods.cairo_show_text (state, str);
		}		

		public void TextPath (string str)
		{
			NativeMethods.cairo_text_path  (state, str);
		}		

		public TextExtents TextExtents (string utf8)
		{
			TextExtents extents;
			NativeMethods.cairo_text_extents (state, utf8, out extents);
			return extents;
		}

		public TextExtents GlyphExtents (Glyph[] glyphs)
		{
			IntPtr ptr = Glyph.FromGlyphToUnManagedMemory (glyphs);
			TextExtents extents;
			NativeMethods.cairo_glyph_extents (state, ptr, glyphs.Length, out extents);
			Marshal.FreeHGlobal (ptr);
			return extents;
		}

#if COMPAT
		[Obsolete ("Use UserToDevice method")]
		public void TransformPoint (ref double x, ref double y)
		{
			UserToDevice (ref x, ref y);
		}

		[Obsolete ("Use UserToDeviceDistance method")]
		public void TransformDistance (ref double dx, ref double dy)
		{
			UserToDeviceDistance (ref dx, ref dy);
		}

		[Obsolete ("Use DeviceToUser method")]
		public void InverseTransformPoint (ref double x, ref double y)
		{
			DeviceToUser (ref x, ref y);
		}

		[Obsolete ("Use DeviceToUserDistance method")]
		public void InverseTransformDistance (ref double dx, ref double dy)
		{
			DeviceToUserDistance (ref dx, ref dy);
		}

		[Obsolete ("Use SetSource(Color) method")]
		public Cairo.Color Color {
			set { 
				SetSource (value);
			}
		}

		[Obsolete ("Use SetSource(Color) method")]
		public Cairo.Color ColorRgb {
			set { 
				SetSource (value);
			}
		}

		[Obsolete ("Use SetSource(Source,double,double) method")]
		public void SetSourceSurface (Surface source, int x, int y)
		{
			SetSource (source, x, y);
		}

		[Obsolete ("Use FontSize property")]
		public void FontSetSize (double scale)
		{
			SetFontSize (scale);
		}

		[Obsolete("The matrix argument was never used, use ShowGlyphs(Glyphs []) instead")]
		public void ShowGlyphs (Matrix matrix, Glyph[] glyphs)
		{
			ShowGlyphs (glyphs);
		}

		[Obsolete("The matrix argument was never used, use GlyphPath(Glyphs []) instead")]
		public void GlyphPath (Matrix matrix, Glyph[] glyphs)
		{
			GlyphPath (glyphs);
		}

		/*
		[Obsolete ("Use SelectFontFace() instead.")]
		public void FontFace (string family, FontSlant slant, FontWeight weight)
		{
			SelectFontFace (family, slant, weight);
		}
		*/

		//FIXME: why obsolete?
		public Pattern Pattern {
			get {
				return Source;
			}
			set {
				SetSource (value);
			}
		}
#endif
	}

#if COMPAT
	[Obsolete ("Renamed Cairo.Context per suggestion from cairo binding guidelines.")]
	public class Graphics : Context
	{
		public Graphics (IntPtr state) : base (state) {}
		public Graphics (Surface surface) : base (surface) {}
	}
#endif
}
